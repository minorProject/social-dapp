import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";
import { Nav, Navbar, NavItem } from "react-bootstrap";
import UpVoteContract from "./contracts/Upvote.json";
import getWeb3 from "./utils/getWeb3";
import Routes from "./Routes";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ipfsHash: null,
      web3: null,
      account: null,
      buffer: null,
      contract: null
    };
  }

  componentDidMount = async () => {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3
      .then(results => {
        this.setState({
          web3: results.web3
        });

        // Instantiate contract once web3 provided.
        this.instantiateContract();
      })
      .catch(() => {
        console.log("Error finding web3.");
      });
  };

  instantiateContract() {
    /*
     * SMART CONTRACT EXAMPLE
     *
     * Normally these functions would be called in the context of a
     * state management library, but for convenience I've placed them here.
     */

    const contract = require("truffle-contract");
    const UpVote = contract(UpVoteContract);
    UpVote.setProvider(this.state.web3.currentProvider);

    // Get accounts.
    var eth = this.state.web3.eth;
    this.state.web3.eth.getAccounts((error, accounts) => {
      UpVote.deployed().then(instance => {
        this.UpVoteInstance = instance;
        this.setState({ account: accounts[0] });
        eth.defaultAccount = accounts[0];
      });
    });
  }

  render() {
    return (
      <div className="App">
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/login"> Login </Link>
            </Navbar.Brand>
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <NavItem>
                <NavLink to="/home">Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="/create">Create</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="/user_profile">Profile</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="/dashboard">Dashboard</NavLink>
              </NavItem>
            </Nav>
            <img
              src="../public/img/user.png"
              alt=""
              align="right"
              height="50"
              width="50"
            />
          </Navbar.Collapse>
        </Navbar>
        <Routes />
      </div>
    );
  }
}

export default App;
