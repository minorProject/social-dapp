pragma solidity 0.5.0;
contract Token {
    
    mapping (address => uint256) public balance;

    
    constructor( uint256 initialAmt) public payable {
        balance[msg.sender] = initialAmt;
    }

    /* Send coins */
    function transfer(address _to, uint256 _value) public payable{
        require(balance[msg.sender] >= _value);           // Check if the sender has enough
        balance[msg.sender] -= _value;                    // Subtract from the sender
        balance[_to] += _value;                           // Add the same to the recipient
    }
}

