pragma solidity 0.5.0;
contract Upvote{
    
    mapping (string => address) map_creator_cont;
    string[] cont_refs;
    address addr_owner;
    
    constructor() public {
    }
    
    function bindCont(string memory post_ref) public{
        map_creator_cont[post_ref] = msg.sender;
        cont_refs.push(post_ref);
    }
    
    function getOwnerAddr(string memory post_ref) public view returns (address){
        return map_creator_cont[post_ref];
    }
    
    
    function getArrLength()public view returns (uint256){
        return cont_refs.length;
    }
    
    function getContRef(uint256 idx) public view returns(string memory)
    {
        return cont_refs[idx];
    }

    function checkOwnership(string memory post_ref) public view returns (uint256){
        if( msg.sender == address(map_creator_cont[post_ref]) )
            return 1;

        else return 0;
    }
}