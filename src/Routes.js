import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home";
import Login from "./containers/Login";
import Create from "./containers/Create";
import Profile from "./containers/Profile";
import Dashboard from "./containers/Dashboard";
import Post from "./containers/Post";

export default () => (
  <Switch>
    <Route exact path="/home" component={Home} />
    <Route path="/login" component={Login} />
    <Route path="/create" component={Create} />
    <Route path="/user_profile" component={Profile} />
    <Route path="/dashboard" component={Dashboard} />
    <Route path="/post" component={Post} />
  </Switch>
);
