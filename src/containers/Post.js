import React, { Component } from "react";
import UpVoteContract from "../contracts/Upvote.json";
import TokenContract from "../contracts/Token.json";
import getWeb3 from "../utils/getWeb3";
import ipfs from "../ipfs";
import { Jumbotron, Button } from "react-bootstrap";
import { Card, CardImg } from "react-bootstrap-card";

export class Post extends Component {
  constructor(props) {
    super(props);

    this.state = {
      web3: null,
      account: null,
      contract: null,
      data: null,
      content_ref: null,
      creator: null
    };

    this.getData = this.getData.bind(this);
    this.votePost = this.votePost.bind(this);
  }
  componentDidMount = async () => {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.
    getWeb3
      .then(results => {
        this.setState({
          web3: results.web3
        });

        // Instantiate contract once web3 provided.
        this.instantiateContract();
      })
      .catch(() => {
        console.log("Error finding web3.");
      });
  };

  instantiateContract() {
    /*
     * SMART CONTRACT EXAMPLE
     *
     * Normally these functions would be called in the context of a
     * state management library, but for convenience I've placed them here.
     */

    const contract = require("truffle-contract");
    const UpVote = contract(UpVoteContract);
    const Token = contract(TokenContract);
    UpVote.setProvider(this.state.web3.currentProvider);
    Token.setProvider(this.state.web3.currentProvider);

    // Get accounts.
    var eth = this.state.web3.eth;
    this.state.web3.eth.getAccounts((error, accounts) => {
      UpVote.deployed().then(instance => {
        this.UpVoteInstance = instance;
        this.setState({ account: accounts[0] });
        console.log(
          "instantiateContract success in post",
          "set account",
          this.state.account
        );
        // Get the value from the contract to prove it worked.
        eth.defaultAccount = accounts[0];
      });
      Token.deployed().then(instance1 => {
        this.TokenInstance = instance1;
      });
      this.getData(this.props.cont_ref);
    });
  }
  getData(content_ref) {
    var data;
    ipfs.cat(content_ref, (err, new_data) => {
      if (err) {
        return console.error("ipfs cat error ", err);
      }
      data = new_data.toString().slice(0, 50);
      console.log(content_ref);
      // console.log("DATA:", data);
      this.setState({ data: data });
      this.setState({ content_ref: content_ref });
    });
    return (
      <div>
        {/* <p>{this.state.data}</p> */}
        {/* <img
          src={`https://ipfs.io./ipfs/${this.content_ref.toString()}`}
          alt=""
        /> */}
      </div>
    );
  }

  votePost(event) {
    event.preventDefault();
    this.UpVoteInstance.getCreatorAddress(
      this.state.content_ref,
      (err, creator_address) => {
        this.setState({ creator: creator_address });
        this.TokenInstance.transfer(creator_address, 20, {
          from: this.state.account
        });
        console.log("successfully tokens transferred");
      }
    )
      .then(r => {
        window.alert(
          `successfully tokens transferred to ${this.state.creator}`
        );
      })
      .catch(error => {
        console.error(error);
      });
  }
  render() {
    return (
      <div>
        <Jumbotron fluid>
          <h4>Post Owner: {this.state.creator} </h4>
          <Card>
            <CardImg
              variant="top"
              src={`https://ipfs.io./ipfs/${this.props.cont_ref.toString()}`}
              alt=""
            />
          </Card>
          <h3>{this.state.data}</h3>
          <br />
          <Button variant="secondary" size="lg" onClick={this.votePost} active>
            Like
          </Button>
        </Jumbotron>
      </div>
    );
  }
}

export default Post;
