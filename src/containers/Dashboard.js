import React, { Component } from "react";
import UpVoteContract from "../contracts/Upvote.json";
import getWeb3 from "../utils/getWeb3";
import { Card, CardImg, CardText } from "react-bootstrap-card";

export class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ipfsHash: null,
      storageValue: 0,
      web3: null,
      account: null,
      buffer: null,
      contract: null,
      text: null
    };
  }
  componentDidMount = async () => {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3
      .then(results => {
        this.setState({
          web3: results.web3
        });

        // Instantiate contract once web3 provided.
        this.instantiateContract();
      })
      .catch(() => {
        console.log("Error finding web3.");
      });
  };

  instantiateContract() {
    /*
     * SMART CONTRACT EXAMPLE
     *
     * Normally these functions would be called in the context of a
     * state management library, but for convenience I've placed them here.
     */

    const contract = require("truffle-contract");
    const UpVote = contract(UpVoteContract);
    UpVote.setProvider(this.state.web3.currentProvider);

    // Get accounts.
    var eth = this.state.web3.eth;
    this.state.web3.eth.getAccounts((error, accounts) => {
      UpVote.deployed().then(instance => {
        this.UpVoteInstance = instance;
        this.setState({ account: accounts[0] });
        // Get the value from the contract to prove it worked.
        eth.defaultAccount = accounts[0];
      });
    });
  }
  render() {
    return (
      <div>
        <Card className="text-center" text="white">
          <CardImg variant="top" />
          <CardText text="white">
            Ethereum is a programmable blockchain. Bitcoin intends to
            decentralized the currency, and Ethereum intends to decentralize the
            applications. Ethereum does this by building what is essentially the
            ultimate abstract foundational layer: a blockchain with a built-in
            Turing-complete programming language, allowing anyone to write smart
            contracts and decentralized applications where they can create their
            own arbitrary rules for ownership, transaction formats and state
            transition functions.
            <b>Social DApp</b> is a decentralized application where you own the
            content you create and can share and get paid without any middlemen.
            It seeks to create a community where the creators of the content as
            well as the readers are directly benefiting without a central entity
            (like Medium).
          </CardText>
        </Card>
        <hr />
      </div>
    );
  }
}

export default Dashboard;
