import React, { Component } from "react";
import UpVoteContract from "../contracts/Upvote.json";
import getWeb3 from "../utils/getWeb3";
import "./Home.css";
import Post from "./Post.js";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ipfsHash: null,
      storageValue: 0,
      web3: null,
      account: null,
      buffer: null,
      contract: null,
      content_ref: []
    };

    this.getArray = this.getArray.bind(this);
    // this.getData = this.getData.bind(this);
    // this.captureText = this.captureText.bind(this);
  }

  componentDidMount = async () => {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3
      .then(results => {
        this.setState({
          web3: results.web3
        });

        // Instantiate contract once web3 provided.
        this.instantiateContract();
      })
      .catch(() => {
        console.log("Error finding web3.");
      });
  };

  instantiateContract() {
    const contract = require("truffle-contract");
    const UpVote = contract(UpVoteContract);

    UpVote.setProvider(this.state.web3.currentProvider);

    // Get accounts.
    var eth = this.state.web3.eth;
    this.state.web3.eth.getAccounts((error, accounts) => {
      UpVote.deployed().then(instance => {
        this.UpVoteInstance = instance;
        this.setState({ account: accounts[0] });
        // Get the value from the contract to prove it worked.
        eth.defaultAccount = accounts[0];
        console.log(
          "instantiateContract success",
          "set account",
          this.state.account
        );
        this.getArray();
      });
    });
  }
  getArray() {
    this.UpVoteInstance.getArrayLength((err, res) => {
      console.log(res);
      for (var i = 0; i < res; i++) {
        this.UpVoteInstance.getContentRef(i, (err, new_ref) => {
          this.setState({
            content_ref: [...this.state.content_ref, new_ref]
          });
        });
      }
      console.log("got array 2 in home", this.state.content_ref);
    });
  }

  render() {
    return (
      <div className="Home">
        <h1>Welcome to Home!</h1>
        {/* <h2 className="text-left">Your Posts.</h2> */}
        <h4 className="text-left">
          This data is stored on IPFS and the Ethereum Blockchain.
        </h4>
        {this.state.content_ref.map(content_ref => (
          <Post key={content_ref} cont_ref={content_ref} />
        ))}
      </div>
    );
  }
}

export default Home;
