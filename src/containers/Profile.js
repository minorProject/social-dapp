import React, { Component } from "react";
import UpVoteContract from "../contracts/Upvote.json";
import getWeb3 from "../utils/getWeb3";
import Post from "./Post.js";

export class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ipfsHash: null,
      storageValue: 0,
      web3: null,
      account: null,
      buffer: null,
      contract: null,
      content_ref: [],
      creator: "0x3fBAA94ff80F85C62E707122c412B44963063adB"
    };

    this.getArray = this.getArray.bind(this);
    this.getCreator = this.getCreator.bind(this);
  }

  componentDidMount = async () => {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3
      .then(results => {
        this.setState({
          web3: results.web3
        });

        // Instantiate contract once web3 provided.
        this.instantiateContract();
      })
      .catch(() => {
        console.log("Error finding web3.");
      });
  };

  instantiateContract() {
    const contract = require("truffle-contract");
    const UpVote = contract(UpVoteContract);

    UpVote.setProvider(this.state.web3.currentProvider);

    // Get accounts.
    var eth = this.state.web3.eth;
    this.state.web3.eth.getAccounts((error, accounts) => {
      UpVote.deployed().then(instance => {
        this.UpVoteInstance = instance;
        this.setState({ account: accounts[0] });
        // Get the value from the contract to prove it worked.
        eth.defaultAccount = accounts[0];
        console.log(
          "instantiateContract success",
          "set account",
          this.state.account
        );
        this.getArray();
      });
    });
  }

  getArray() {
    this.UpVoteInstance.getArrayLength((err, res) => {
      console.log(res);
      for (var i = 0; i < res; i++) {
        this.UpVoteInstance.getContentRef(i, (err, new_ref) => {
          this.UpVoteInstance.checkOwnership(
            new_ref,
            (err, ownership_status) => {
              if (ownership_status) {
                console.log(new_ref);
                this.setState({
                  content_ref: [...this.state.content_ref, new_ref]
                });
              } else console.log("not an owner");
            }
          );
        });
      }
      this.getCreator();
      console.log("got array 2 in profile", this.state.content_ref);
    }).then(res => {
      this.getCreator();
    });
  }

  getCreator() {
    this.UpVoteInstance.getCreatorAddress(
      this.state.content_ref[0],
      (err, creator_address) => {
        this.setState({ creator: creator_address });
      }
    );
  }

  render() {
    return (
      <div className="Profile">
        <h1>Welcome to Your Profile!</h1>
        <h2 className="text-left">Your Posts.</h2>
        <h4 className="text-left">
          This data is stored on IPFS and the Ethereum Blockchain in relation to
          the account : {this.state.creator}
        </h4>
        {this.state.content_ref.map(content_ref => (
          <Post key={content_ref} cont_ref={content_ref} />
        ))}
      </div>
    );
  }
}

export default Profile;
