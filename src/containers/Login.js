import React, { Component } from "react";
import {
  Form,
  FormControl,
  FormGroup,
  Button,
  ControlLabel
} from "react-bootstrap";
import { Redirect } from "react-router-dom";
// import { Auth } from "aws-amplify";

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      acc_addr: "",
      password: "",
      formGroupEmail: "",
      email: "",
      redirect: false
    };

    this.validateForm = this.validateForm.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.setRedirect = this.setRedirect.bind(this);
  }

  setRedirect() {
    this.setState({
      redirect: true
    });
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/home" />;
    }
  };

  validateForm() {
    return (
      (this.state.email.length > 0 || this.state.acc_addr.length > 0) &&
      this.state.password.length > 0
    );
  }

  handleSubmit = async event => {
    event.preventDefault();
    console.log("submitted");
    console.log(this.state.email, this.state.password);
    try {
      // await Auth.signIn(this.state.email, this.state.password);
      alert("Logged in");
    } catch (e) {
      alert(e.message);
    }
    this.setRedirect();
  };

  handleChange(event) {
    this.setState({
      [event.target.id]: event.target.value
    });

    console.log(this.state.email, this.state.password);
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              size="lg"
              type="email"
              placeholder="Enter email"
              onChange={this.handleChange}
            />
          </FormGroup>
          <h3>OR</h3>
          <FormGroup controlId="acc_addr">
            <ControlLabel>Ethereum Address</ControlLabel>
            <FormControl
              size="lg"
              type="text"
              placeholder="Enter address"
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              size="lg"
              type="password"
              placeholder="Password"
              onChange={this.handleChange}
            />
          </FormGroup>
          <Button
            variant="primary"
            type="submit"
            disabled={!this.validateForm()}
          >
            Login
          </Button>
        </Form>
        {this.renderRedirect()}
      </div>
    );
  }
}

export default Login;
