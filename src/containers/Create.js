import React, { Component } from "react";
import UpVoteContract from "../contracts/Upvote.json";
import getWeb3 from "../utils/getWeb3";
import ipfs from "../ipfs";
import { Form, FormControl, FormGroup, Button } from "react-bootstrap";
// import Editor from "react-medium-editor";

class Create extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ipfsHash: null,
      storageValue: 0,
      web3: null,
      account: null,
      buffer: null,
      contract: null,
      text: "What's on your mind?"
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.captureFile = this.captureFile.bind(this);
    this.captureText = this.captureText.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount = async () => {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3
      .then(results => {
        this.setState({
          web3: results.web3
        });

        this.instantiateContract();
      })
      .catch(() => {
        console.log("Error finding web3.");
      });
  };

  instantiateContract() {
    const contract = require("truffle-contract");
    const UpVote = contract(UpVoteContract);
    UpVote.setProvider(this.state.web3.currentProvider);

    // Get accounts.
    var eth = this.state.web3.eth;
    this.state.web3.eth.getAccounts((error, accounts) => {
      UpVote.deployed().then(instance => {
        this.UpVoteInstance = instance;
        this.setState({ account: accounts[0] });
        // Get the value from the contract to prove it worked.
        eth.defaultAccount = accounts[0];
        console.log("instantiateContract success");
      });
    });
  }

  captureFile(event) {
    event.preventDefault();
    const file = event.target.files[0];
    const reader = new window.FileReader();
    reader.readAsArrayBuffer(file);
    reader.onloadend = () => {
      this.setState({ buffer: Buffer(reader.result) });
      console.log("buffer", this.state.buffer);
    };
  }

  captureText(event) {
    event.preventDefault();
    this.setState({ text: event.target.value });
    let content = ipfs.types.Buffer.from(event.target.value);
    this.setState({ buffer: content });
    console.log("text", this.state.buffer, this.state.text);
  }

  handleChange(text, medium) {
    this.setState({ text: text });
  }

  onSubmit(event) {
    event.preventDefault();
    ipfs.add(this.state.buffer, (error, result) => {
      if (error) {
        console.error(error);
        return;
      }
      console.log("Data added to ipfs and contract");
      console.log("ifpsHash", result[0].hash);
      this.setState({ ipfsHash: result[0].hash });
      this.UpVoteInstance.bind(result[0].hash, {
        from: this.state.account
      }).then(r => {
        console.log("binding done");
        window.alert("Successfully Added to IPFS and Blockchain");
      });
    });
  }

  render() {
    return (
      <div className="Create">
        <h1>Write Post or Upload Files!</h1>
        <p>This data is stored on IPFS and the Ethereum Blockchain</p>
        <Form className="text-center" onSubmit={this.onSubmit}>
          <FormGroup>
            <h3 className="text-center">Upload Image or Video</h3>
            <FormControl
              type="file"
              onChange={this.captureFile}
              placeholder="Choose File"
            />
          </FormGroup>
          <FormGroup>
            <h3 className="text-center">Write Something</h3>
            <FormControl
              type="text"
              name="data"
              value={this.state.text}
              onChange={this.captureText}
              placeholder="Type"
            />
          </FormGroup>
          {/* <Editor
            tag="pre"
            value={this.state.text}
            onChange={this.handleChange}
            options={{ toolbar: { buttons: ["bold", "italic", "underline"] } }}
          /> */}
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </div>
    );
  }
}

export default Create;
